<?php

function doTask($json1, $json2, $json3) {
    $result = [];

    foreach ($json3["captures"] as $test) {
        $time = strtotime($test["time"]);
        $result[$time] = [
            "expected" => $test["expected"],
            "actual" => $test["actual"]
        ];
    }

    foreach ($json1["logs"] as $test) {
        $time = (int) $test["time"];
        if (!isset($result[$time])) {
            continue;
        }

        $result[$time]["name"] = $test["test"];
        $result[$time]["result"] = $test["test"];
    }

    foreach ($json2["suites"] as $suites) {
        foreach ($suites["cases"] as $case) {
            $time = strtotime($case["time"]);
            if (!isset($result[$time])) {
                continue;
            }

            $result[$time]["name"] = $case["name"];
            $result[$time]["result"] = $case["erros"] > 0 ? "fail" : "success";
        }
    }

    $result = array_values($result);
    $resultFiltered = [];

    foreach ($result as $r) {
        if (isset($r["name"]) &&
            isset($r["result"]) &&
            isset($r["expected"]) &&
            isset($r["actual"])
        ) {
            $resultFiltered[] = $r;
        }
    }

    return $resultFiltered;
}

function read($file) {
    $data = file_get_contents($file);
    if ($data == false) {
        return false;
    }

    $json = json_decode($data, true);
    if ($json == null) {
        return false;
    }

    return $json;
}

if (($json1 = read("./1.json")) == false) {
    echo "Ошибка чтения 1.json";
    return;
}
if (($json2 = read("./2.json")) == false) {
    echo "Ошибка чтения 2.json";
    return;
}
if (($json3 = read("./3.json")) == false) {
    echo "Ошибка чтения 3.json";
    return;
}

$result = doTask($json1, $json2, $json3);

file_put_contents("./result.json", json_encode($result, JSON_PRETTY_PRINT));
